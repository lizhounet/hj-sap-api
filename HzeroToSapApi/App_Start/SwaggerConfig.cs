using System.Web.Http;
using WebActivatorEx;
using HzeroToSapApi;
using Swashbuckle.Application;

[assembly: PreApplicationStartMethod(typeof(SwaggerConfig), "Register")]

namespace HzeroToSapApi
{
    public class SwaggerConfig
    {
        public static void Register()
        {
            var thisAssembly = typeof(SwaggerConfig).Assembly;

            GlobalConfiguration.Configuration
                .EnableSwagger(c =>
                    {
                        c.SingleApiVersion("v1", "HzeroToSapApi");
                        c.IncludeXmlComments(GetXmlCommentsPath());
                        c.SchemaId(x => x.FullName);
                    })
                .EnableSwaggerUi(c =>
                    {
                    
                    });
        }
        /// <summary>
        /// ��ȡxml
        /// </summary>
        /// <returns></returns>
        protected static string GetXmlCommentsPath() {
            return string.Format($@"{System.AppDomain.CurrentDomain.BaseDirectory}\bin\HzeroToSapApi.xml");
        }
    }
    
}
