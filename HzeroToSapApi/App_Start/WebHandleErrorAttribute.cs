﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Http.Filters;
using System.Web.Mvc;

namespace HzeroToSapApi.App_Start
{
    public class WebHandleErrorAttribute: ExceptionFilterAttribute
    {

        public override void OnException(HttpActionExecutedContext filterContext)
        {
            base.OnException(filterContext);

            HttpResponseMessage httpResponse = new HttpResponseMessage();
            httpResponse.Content = new StringContent(JsonConvert.SerializeObject(new
            {
                 code=500,
                 message= filterContext.Exception.ToString()
            }));
            filterContext.Response = httpResponse;
        }
    }
}