﻿using HzeroToSapApi.BpcSubjectOver;
using HzeroToSapApi.SapSalesPrice;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Http;

namespace HzeroToSapApi.Controllers
{
    /// <summary>
    /// BPC 科目余额查询接口
    /// </summary>
    public class BpcSubjectOverController : ApiController
    {
        private SI_BWD_BPC_IF004_Out_SynClient synClient = new SI_BWD_BPC_IF004_Out_SynClient();
        public BpcSubjectOverController()
        {
            synClient.ClientCredentials.UserName.UserName = ConfigurationManager.AppSettings["SapAccount"];
            synClient.ClientCredentials.UserName.Password = ConfigurationManager.AppSettings["SapPassword"];
        }

        /// <summary>
        /// BPC 科目余额查询
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public async Task<SI_BWD_BPC_IF004_Out_SynResponse> Post([FromBody] SI_BWD_BPC_IF004_Out_SynRequest request)
        {
            return await synClient.SI_BWD_BPC_IF004_Out_SynAsync(request);
        }
    }
}
