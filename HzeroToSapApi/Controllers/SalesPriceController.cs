﻿using HzeroToSapApi.SapSalesPrice;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace HzeroToSapApi.Controllers
{
    /// <summary>
    /// 销售价格接口
    /// </summary>
    public class SalesPriceController : ApiController
    {
        private SI_PAAS_SD_IF002_Out_SynClient synClient = new SI_PAAS_SD_IF002_Out_SynClient();
        public SalesPriceController()
        {
            synClient.ClientCredentials.UserName.UserName = ConfigurationManager.AppSettings["SapAccount"];
            synClient.ClientCredentials.UserName.Password = ConfigurationManager.AppSettings["SapPassword"];
        }
   
        /// <summary>
        /// 销售价格创建
        /// </summary>
        /// <param name="inPut"></param>
        /// <returns></returns>
        public Z03PAAS_SD_IF002_S_OUT Post([FromBody] Z03PAAS_SD_IF002_S_IN inPut)
        {
            var synResponse =  synClient.SI_PAAS_SD_IF002_Out_Syn(inPut);
            return synResponse;
        }
    }
}
