﻿using HzeroToSapApi.BpcBudget;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace HzeroToSapApi.Controllers
{
    /// <summary>
    /// BPC 年度预算与与调整接口
    /// </summary>
    public class BpcBudgetController : ApiController
    {
        private SI_BWD_BPC_IF002_Out_SynClient synClient = new SI_BWD_BPC_IF002_Out_SynClient();
        public BpcBudgetController()
        {
            synClient.ClientCredentials.UserName.UserName = ConfigurationManager.AppSettings["SapAccount"];
            synClient.ClientCredentials.UserName.Password = ConfigurationManager.AppSettings["SapPassword"];
        }

        /// <summary>
        /// BPC 年度预算与与调整接口
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public async Task<SI_BWD_BPC_IF002_Out_SynResponse> Post([FromBody] SI_BWD_BPC_IF002_Out_SynRequest request)
        {
            return await synClient.SI_BWD_BPC_IF002_Out_SynAsync(request);
        }
    }
}
