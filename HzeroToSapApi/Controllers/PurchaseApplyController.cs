﻿using HzeroToSapApi.SapPurchaseApply;
using System.Configuration;
using System.Web.Http;

namespace HzeroToSapApi.Controllers
{
    /// <summary>
    /// 采购申请
    /// </summary>
    public class PurchaseApplyController : ApiController
    {
        private SI_PAAS_MM_IF006_Out_SynClient synClient = new SI_PAAS_MM_IF006_Out_SynClient();
        public PurchaseApplyController()
        {
            synClient.ClientCredentials.UserName.UserName = ConfigurationManager.AppSettings["SapAccount"];
            synClient.ClientCredentials.UserName.Password = ConfigurationManager.AppSettings["SapPassword"];
        }
   
        /// <summary>
        /// 采购申请创建
        /// </summary>
        /// <param name="inPut"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("purchase-apply")]
        public Z03PAAS_MM_IF006_S_OUT PurchaseInfoCreate([FromBody] Z03PAAS_MM_IF006_S_IN inPut)
        {
            var synResponse = synClient.SI_PAAS_MM_IF006_Out_Syn(inPut);
            return synResponse;
        }
    }
}
