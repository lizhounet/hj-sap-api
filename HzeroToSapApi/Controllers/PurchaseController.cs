﻿using HzeroToSapApi.SapPurchaseInfo;
using HzeroToSapApi.SapPurchaseOrder;
using System.Configuration;
using System.Web.Http;

namespace HzeroToSapApi.Controllers
{
    /// <summary>
    /// 采购接口
    /// </summary>
    public class PurchaseController : ApiController
    {
        private SI_PAAS_MM_IF003_Out_SynClient synPurchaseInfoClient = new SI_PAAS_MM_IF003_Out_SynClient();
        private SI_PAAS_MM_IF005_Out_SynClient synPurchaseOrderClient = new SI_PAAS_MM_IF005_Out_SynClient();
        public PurchaseController()
        {
            synPurchaseInfoClient.ClientCredentials.UserName.UserName = ConfigurationManager.AppSettings["SapAccount"];
            synPurchaseInfoClient.ClientCredentials.UserName.Password = ConfigurationManager.AppSettings["SapPassword"];
            synPurchaseOrderClient.ClientCredentials.UserName.UserName = ConfigurationManager.AppSettings["SapAccount"];
            synPurchaseOrderClient.ClientCredentials.UserName.Password = ConfigurationManager.AppSettings["SapPassword"];
        }
   
        /// <summary>
        /// 采购信息记录创建
        /// </summary>
        /// <param name="inPut"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("purchase-info")]
        public Z03PAAS_MM_IF003_S_OUT PurchaseInfoCreate([FromBody] Z03PAAS_MM_IF003_S_IN inPut)
        {
            var synResponse = synPurchaseInfoClient.SI_PAAS_MM_IF003_Out_Syn(inPut);
            return synResponse;
        }
        /// <summary>
        /// 采购订单审批回传
        /// </summary>
        /// <param name="inPut"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("purchase-order")]
        public Z03PAAS_MM_IF005_S_OUT PurchaseOrder([FromBody] Z03PAAS_MM_IF005_S_IN inPut)
        {
            var synResponse = synPurchaseOrderClient.SI_PAAS_MM_IF005_Out_Syn(inPut);
            return synResponse;
        }
    }
}
